// ES6 VERSION
let sideMenu = document.querySelector(".app-menu-content");
let btnMenu = document.querySelector(".btn-menu");
let menuImg = document.querySelector(".app-logo");
let layout = document.querySelector(".layout");

const baseLogoPath = "/StyleGuidePreview/img/StyleGuidePreview.";
const expandedLogo = `${baseLogoPath}Logoexpanded.png`;
const iconLogo = `${baseLogoPath}LogoIcon.png`;

btnMenu.onClick = () => {
    sideMenu.classList.toggle("open");
    layout.classList.toggle("menu-open");

    if(sideMenu.classList.contains("open")) {
        menuImg.attr("src", expandedLogo);
    }else{
        menuImg.attr("src", iconLogo);
    }
}

//ES5 VERSION:
btnMenu.onClick = function () {
    sideMenu.classList.toggle("open");
    layout.classList.toggle("menu-open");

    if (sideMenu.classList.contains("open")) {
        menuImg.attr("src", expandedLogo);
    } else {
        menuImg.attr("src", iconLogo);
    }
};