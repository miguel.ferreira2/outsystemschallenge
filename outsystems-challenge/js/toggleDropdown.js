// ES6 VERSION
let topMenu = document.querySelector(".app-menu-content");
let toggleDropdownLink = document.querySelector('a[data-element="user-avatar"]');
let dropdown = document.querySelector('div[data-element="userActionsDropdown"]');

const baseLogoPath = "/StyleGuidePreview/img/StyleGuidePreview.";
const expandedLogo = `${baseLogoPath}Logoexpanded.png`;
const iconLogo = `${baseLogoPath}LogoIcon.png`;

btnMenu.onClick = () => {
    dropdown.classList.toggle("open-dropdown");
}

//ES5 VERSION:
btnMenu.onClick = function () {
    dropdown.classList.toggle("open-dropdown");
};